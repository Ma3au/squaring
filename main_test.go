package main

import (
	"reflect"
	"testing"
)

// Создаем канал chIn, массив, запускаем горутину и добавляем числа из chIn в массив,
// затем проверяем, что длина массива равна 10
func TestRandInt100(t *testing.T) {
	chIn := make(chan int)
	var result []int

	go RandInt100(chIn)

	for num := range chIn {
		result = append(result, num)
	}

	if len(result) != 10 {
		t.Errorf("Expected length of result to be 10, but got %d", len(result))
	}
}

// Создаем два канала chIn и chOut, в анонимной горутине передаем в chIn два числа,
// запускаем горутину и создаем массив, куда добавляем числа из канала chOut
// создаем массив с верными результатами и сравниваем с массивом чисел из канала chOut
func TestSquaring(t *testing.T) {
	chIn := make(chan int)
	chOut := make(chan int)

	go func() {
		chIn <- 2
		chIn <- 3
		close(chIn)
	}()

	go Squaring(chIn, chOut)

	var result []int
	for num := range chOut {
		result = append(result, num)
	}

	expected := []int{4, 9}
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
}
