package main

import (
	"fmt"
	"math/rand"
	"time"
)

// Функция, которая рандомно генерирует 10 положительных чисел в диапозоне [0;100)
// и посылает их в канал chIn
func RandInt100(chIn chan int) {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 10; i++ {
		chIn <- rand.Intn(100)
	}
	close(chIn)
}

// Функция, которая принимает числа из канала chIn, пока он не закрыт и отправляет
// квадрат чисел в канал chOut
func Squaring(chIn chan int, chOut chan int) {
	for num := range chIn {
		chOut <- num * num
	}
	close(chOut)
}

func main() {
	// Создаем два канала для входа и выхода
	chIn := make(chan int)
	chOut := make(chan int)

	// Запускаем две горутины
	go RandInt100(chIn)
	go Squaring(chIn, chOut)

	// Выводим квадрат чисел из канала chOut на печать пока он не закроется
	for num := range chOut {
		fmt.Println(num)
	}
}
